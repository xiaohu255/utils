package com.utils.v1.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * InputStream 工具包
 * @author: xiaohu
 * @Date: 2021年5月26日 上午10:15:33
 * @Description:
 */
public class InputStreamUtils {
	
	private static final int DEFAULT_BUFFER_SIZE = 1024 * 4;
	
	/**
	 * 从输入流拷贝信息到输出流
	 * @Date: 2021年5月26日 上午10:13:34
	 * @param input
	 * @param output
	 * @return
	 * @throws IOException
	 * @Description:
	 */
    public static int copy(InputStream input, OutputStream output) throws IOException {
        long count = copyLarge(input, output);
        if (count > Integer.MAX_VALUE) {
            return -1;
        }
        return (int) count;
    }

    /**
     * 从输入流拷贝信息到输出流
     * @Date: 2021年5月26日 上午10:14:06
     * @param input
     * @param output
     * @return
     * @throws IOException
     * @Description:
     */
    public static long copyLarge(InputStream input, OutputStream output)
            throws IOException {
        byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
        long count = 0;
        int n = 0;
        while (-1 != (n = input.read(buffer))) {
            output.write(buffer, 0, n);
            count += n;
        }
        return count;
    }
	
}
