package com.utils.v1.base;

/**
 * String 工具包
 * @author: xiaohu
 * @Date: 2021年5月26日 上午10:14:29
 * @Description:
 */
public class StringUtils {
	
	/**
	 * 检测字符串是否为空，严格模式
	 * @Date: 2021年5月26日 上午9:24:23
	 * @param cs
	 * @return
	 * @Description:
	 */
    public static boolean isBlank(CharSequence cs) {
        int strLen;
        if (cs == null || (strLen = cs.length()) == 0) {
            return true;
        }
        for (int i = 0; i < strLen; i++) {
            if (!Character.isWhitespace(cs.charAt(i))) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * 检测字符串是否不为空，严格模式
     * @Date: 2021年5月26日 上午9:27:58
     * @param cs
     * @return
     * @Description:
     */
    public static boolean isNotBlank(CharSequence cs) {
        return !isBlank(cs);
    }
    
    /**
     * 检测字符串是否为空
     * @Date: 2021年5月26日 上午9:25:57
     * @param cs
     * @return
     * @Description:
     */
    public static boolean isEmpty(CharSequence cs) {
        return cs == null || cs.length() == 0;
    }
    
    /**
     * 检测字符串是否不为空
     * @Date: 2021年5月26日 上午9:28:21
     * @param cs
     * @return
     * @Description:
     */
    public static boolean isNotEmpty(CharSequence cs) {
        return !isEmpty(cs);
    }
    
    /**
     * 检测字符串中是否包含另一个字符串
     * @Date: 2021年5月26日 上午9:37:00
     * @param cs 字符串
     * @param ps 另一个字符串
     * @return
     * @Description:
     */
    public static boolean isContain(CharSequence cs, CharSequence ps) {
    	if(isBlank(cs)) {
    		return false;
    	}
    	if(isEmpty(ps)) {
    		return false;
    	}
    	
    	return cs.toString().contains(ps);
    }
    
    /**
     * 检测字符串中是否不包含另一个字符串
     * @Date: 2021年5月26日 上午9:43:10
     * @param cs 字符串
     * @param ps 另一个字符串
     * @return
     * @Description:
     */
    public static boolean isNotContain(CharSequence cs, CharSequence ps) {
    	return !isContain(cs,ps);
    }
    
    /**
     * 把一个字符串按照另一个字符串作分割点切割
     * @Date: 2021年5月26日 上午9:59:25
     * @param cs 一个字符串
     * @param pm 另一个字符串
     * @return
     * @Description:
     */
    public static String[] split(String cs, String pm) {
    	if(isBlank(cs)) {
    		return new String[0];
    	}
    	if(isEmpty(pm)) {
    		return new String[] {cs};
    	}
    	return cs.split(pm);
    }
    
    /**
     * 拼接字符串
     * @Date: 2021年5月26日 上午10:07:13
     * @param cs
     * @return
     * @Description:
     */
    public static String join(CharSequence... cs) {
    	if(cs.length < 1) {
    		return "";
    	}
    	StringBuilder sb = new StringBuilder();
    	for(CharSequence c : cs) {
    		sb.append(c);
    	}
    	return sb.toString();
    }
	
}
