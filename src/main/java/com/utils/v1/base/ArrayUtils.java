package com.utils.v1.base;

import java.lang.reflect.Array;

/**
 * 数组工具包
 * @author: xiaohu
 * @Date: 2021年5月26日 上午11:43:02
 * @Description:
 */
public class ArrayUtils <T> {
	/**
	 * 数组合并
	 * @Date: 2021年5月26日 下午1:38:34
	 * @param <T> 数组类型
	 * @param t 数组类型
	 * @param a 数组a
	 * @param b 数组b
	 * @return
	 * @Description:
	 */
	public static <T> T[] merge(Class<T> t, T[] a, T[] b) {
		int aLength = a.length;
		int bLength = b.length;
		T[] tArr = (T[]) Array.newInstance(t, aLength + bLength);
		System.arraycopy(a, 0, tArr, 0, aLength);
		System.arraycopy(b, 0, tArr, aLength, bLength);
		return tArr;
	}
	
	/**
	 * 合并两个Object数组
	 * @Date: 2021年5月26日 下午1:40:34
	 * @param a 数组a
	 * @param b 数组b
	 * @return
	 * @Description:
	 */
	public static Object[] merge(Object[] a, Object[] b) {
		int aLength = a.length;
		int bLength = b.length;
		Object[] tArr = new Object[aLength + bLength];
		System.arraycopy(a, 0, tArr, 0, aLength);
		System.arraycopy(b, 0, tArr, aLength, bLength);
		return tArr;
	}
}
