package com.utils.v1.base;

/**
 * 数字类型工具包-父包
 * @author: xiaohu
 * @Date: 2021年5月26日 上午10:17:48
 * @Description:
 */
public class NumberUtils {
	
	/**
	 * 判断数字是否是 0, null 也是零
	 * @Date: 2021年5月26日 上午10:23:47
	 * @param nb
	 * @return
	 * @Description:
	 */
	public static boolean isZero(Number nb) {
		return nb == null || nb.intValue() == 0;
	}
	
	/**
	 * 判断数字是否不是 0, null 也是零
	 * @Date: 2021年5月26日 上午10:23:47
	 * @param nb
	 * @return
	 * @Description:
	 */
	public static boolean isNotZero(Number nb) {
		return !isZero(nb);
	}
	
	/**
	 * 判断数字是否是负数, null == 0
	 * @Date: 2021年5月26日 上午10:29:55
	 * @param nb
	 * @return
	 * @Description:
	 */
	public static boolean isMinus(Number nb) {
		if(nb == null) {
			return false;
		}
		return nb.doubleValue() < 0;
	}
	
	/**
	 * 判断数字是否是正数, null == 0
	 * @Date: 2021年5月26日 上午10:33:31
	 * @param nb
	 * @return
	 * @Description:
	 */
	public static boolean isPositive(Number nb) {
		if(nb == null) {
			return false;
		}
		return nb.doubleValue() > 0;
	}
	
	/**
	 * 判断数字是否为null
	 * @Date: 2021年5月26日 上午10:24:59
	 * @param nb
	 * @return
	 * @Description:
	 */
	public static boolean isEmpty(Number nb) {
        return nb == null;
    }
    
    /**
     * 判断数字是否不为null
     * @Date: 2021年5月26日 上午10:25:15
     * @param nb
     * @return
     * @Description:
     */
	public static boolean isNotEmpty(Number nb) {
        return !isEmpty(nb);
    }
	
}
