package com.utils.v1.random;

import java.util.Random;

import com.utils.v1.base.ArrayUtils;
import com.utils.v1.base.IntegerUtils;

/**
 * 随机内容工具包
 * @author: xiaohu
 * @Date: 2021年5月26日 上午10:41:18
 * @Description:
 */
public class RandomUtils {
	
	private static final Character[] KEY_LOWERCASE_26 = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
	
	private static final Character[] KEY_UPPERCASE_26 = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
	
	private static final Character[] KEY_NUMBER = {'0','1','2','3','4','5','6','7','8','9'};
	
	/**
	 * 生成 16 位以下随机数
	 * @Date: 2021年5月26日 上午10:42:20
	 * @param bit
	 * @return
	 * @Description:
	 */
	public static int randomInt(int bit) {
		if(!IntegerUtils.isPositive(bit)) {
			return 0;
		}
		
		if(bit < 16) {
			return randomIntForMath(bit);
		} else {
			return 0;
		}
	}
	
	/**
	 * 生成 16 位以下随机数
	 * @Date: 2021年5月26日 上午10:57:08
	 * @param bit
	 * @return
	 * @Description:
	 */
	private static int randomIntForMath(int bit) {
		int a = 1;
		for(int i = 0; i < bit; i++) {
			a*= 10;
		}
		int res = (int) (Math.random() * a);
		if(res < a/10) {
			res += a/10;
		}
		return res;
	}
	
	/**
	 * 生成指定位数的随机字符串
	 * @Date: 2021年5月26日 下午2:13:00
	 * @param bit
	 * @return
	 * @Description:
	 */
	public static String randomString(int bit) {
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < bit; i++) {
			sb.append(randomChar());
		}
		return sb.toString();
	}
	
	/**
	 * 获取一个随机的 char
	 * @Date: 2021年5月26日 下午2:03:45
	 * @return
	 * @Description:
	 */
	private static char randomChar() {
		Random random = new Random();
		Character[] allKey = ArrayUtils.merge(Character.class, ArrayUtils.merge(Character.class, KEY_LOWERCASE_26, KEY_UPPERCASE_26), KEY_NUMBER);
		int index = random.nextInt(allKey.length);
		return allKey[index];
	}
}
