package com.utils.v1.location;


import java.awt.*;

/**
 * @Name: 经纬度工具
 * @Author: xiaohu
 * @Date 2023/7/11 - 16:04
 * @Description: -
 * 经度 (longitude)
 * 纬度 (latitude)
 * @Version: -
 */
public class LngLatUtils {

    /**
     * 将经纬度转换成整形坐标对象
     * 大概小数点位到后六位就可以精确到 1 米范围
     * @param lng 经度
     * @param lat 纬度
     * @return
     */
    public static Point convertLngLat(double lng, double lat) {
        final int precisionBit = 1000000;
        return new Point((int) (lng * precisionBit), (int) (lat * precisionBit));
    }

}
