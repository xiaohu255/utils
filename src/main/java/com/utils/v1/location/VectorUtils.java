package com.utils.v1.location;

import java.awt.*;

/**
 * @Name: 向量工具
 * @Author: xiaohu
 * @Date 2023/7/11 - 15:31
 * @Description: -
 * @Version: -
 */
public class VectorUtils {

    /**
     * 向量 AB(B.x-A.x, B.y-A.y)
     * @param a
     * @param b
     * @return
     */
    public static Point vector(Point a, Point b) {
        return new Point(b.x - a.x, b.y - a.y);
    }

    /**
     * 向量叉积 AB*AP(B.x-Ax)*(P.y-A.y)-(B.y-A.y)*(P.x-A.x)
     * @param vectorAB 向量AB
     * @param vectorAP 向量AP
     * @return
     */
    public static int vectorCrossProduct(Point vectorAB, Point vectorAP) {
        return vectorAB.x*vectorAP.y-vectorAB.y*vectorAP.x;
    }

    /**
     * 向量叉积AB*AP(B.x-A.x)*(P.y-A.y)-(B.y-A.y)*(P.x-A.x)
     * @param p P 坐标
     * @param a A 坐标
     * @param b B 坐标
     * @return
     */
    public static int vectorCrossProduct(Point p, Point a, Point b) {
        Point vectorAB = vector(a, b);
        Point vectorAP = vector(a, p);
        return vectorCrossProduct(vectorAB, vectorAP);
    }

}
