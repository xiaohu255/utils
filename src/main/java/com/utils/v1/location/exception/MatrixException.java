package com.utils.v1.location.exception;

/**
 * @Name: 矩阵异常
 * @Author: xiaohu
 * @Date 2023/7/18 - 1:33
 * @Description: -
 * @Version: -
 */
public class MatrixException extends Exception {

    public MatrixException(String message) {
        super(message);
    }

}
