package com.utils.v1.location;

import com.utils.v1.location.exception.MatrixException;

import java.awt.*;

/**
 * @Name: 定位工具
 * @Author: xiaohu
 * @Date 2023/7/11 - 15:57
 * @Description: -
 * @Version: -
 */
public class PointUtils {

    /**
     * 判断定位是否在凸矩阵内部, 作废
     * ABCD 矩阵中的四个点位必须是顺序相连的, A -> B -> C -> D
     * @param p 定位
     * @param a 矩阵第一个点
     * @param b 矩阵第二个点
     * @param c 矩阵第三个点
     * @param d 矩阵第四个点
     * @return
     */
    @Deprecated
    public static boolean isPointInRaisedRect(Point p, Point a, Point b, Point c, Point d) {
        // AB X AP = (b.x - a.x, b.y - a.y) x (p.x - a.x, p.y - a.y) = (b.x - a.x) * (py - a.y) - (by - a.y) * (p.x - a.x);
        // BC X BP = (c.x - b.x, c.y - b.y) x (p.x - b.x, p.y - b.y) = (cX - b.x)  (p.y - b.y) - (c.y - b.y) * (p.x - b.x);
        int vectorCrossProductAB = VectorUtils.vectorCrossProduct(p, a, b);
        int vectorCrossProductBC = VectorUtils.vectorCrossProduct(p, b, c);
        int vectorCrossProductCD = VectorUtils.vectorCrossProduct(p, c, d);
        int vectorCrossProductDA = VectorUtils.vectorCrossProduct(p, d, a);
        // 判断向量叉积是否全部都是正数
        if (vectorCrossProductAB > 0 && vectorCrossProductBC > 0 && vectorCrossProductCD > 0 && vectorCrossProductDA > 0) {
            return true;
        }

        // 判断向量叉积是否全部都是负数
        if (vectorCrossProductAB < 0 && vectorCrossProductBC < 0 && vectorCrossProductCD < 0 && vectorCrossProductDA < 0) {
            return true;
        }

        return false;
    }

    /**
     * 判断定位是否在凸矩阵内部
     * 矩阵中的点位必须是顺序相连的, 例如： A -> B -> C -> D
     * @param p 定位
     * @param matrixPoints 点位数组
     * @return
     */
    public static boolean isPointInRaisedRectEx(Point p, Point... matrixPoints) throws MatrixException {
        // AB X AP = (b.x - a.x, b.y - a.y) x (p.x - a.x, p.y - a.y) = (b.x - a.x) * (py - a.y) - (by - a.y) * (p.x - a.x);
        // BC X BP = (c.x - b.x, c.y - b.y) x (p.x - b.x, p.y - b.y) = (cX - b.x)  (p.y - b.y) - (c.y - b.y) * (p.x - b.x);

        if (matrixPoints.length < 3) {
            throw new MatrixException("the matrix needs at least 3 points.");
        }

        int vectorCrossProduct[] = new int[matrixPoints.length];

        for(int i = 0; i < matrixPoints.length; i++) {
            if (i == (matrixPoints.length - 1)) {
                vectorCrossProduct[i] = VectorUtils.vectorCrossProduct(p, matrixPoints[i], matrixPoints[0]);
                continue;
            }
            vectorCrossProduct[i] = VectorUtils.vectorCrossProduct(p, matrixPoints[i], matrixPoints[i+1]);
        }

        Boolean flat = null;

        for(int i = 0; i < vectorCrossProduct.length; i++) {
            if (flat == null) {
                flat = vectorCrossProduct[i] > 0;
                continue;
            }
            if (!flat.equals(vectorCrossProduct[i] > 0)) {
                return false;
            }
        }

        return true;
    }
}
