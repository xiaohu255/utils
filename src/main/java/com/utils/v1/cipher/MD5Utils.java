package com.utils.v1.cipher;

import java.security.MessageDigest;

/**
 * MD5 工具包
 * @author: xiaohu
 * @Date: 2021年5月26日 上午10:14:55
 * @Description:
 */
public class MD5Utils {
	
	/**
	 * 默认编码加密
	 * 
	 * @param s
	 * @return
	 */
	public final static String parse(String s) {
		return parse(s, "UTF-8");
	}
	
	/**
	 * 指定编码加密
	 * 
	 * @param s
	 * @param charset
	 * @return
	 */
	public final static String parse(String s, String charset) {
		char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
		try {
			byte[] strTemp = s.getBytes(charset);
			MessageDigest mdTemp = MessageDigest.getInstance("MD5");
			mdTemp.update(strTemp);
			byte[] md = mdTemp.digest();
			int j = md.length;
			char str[] = new char[j * 2];
			int k = 0;
			for (int i = 0; i < j; i++) {
				byte byte0 = md[i];
				str[k++] = hexDigits[byte0 >>> 4 & 0xf];
				str[k++] = hexDigits[byte0 & 0xf];
			}
			return new String(str);
		} catch (Exception e) {
			return null;
		}
	}
}
